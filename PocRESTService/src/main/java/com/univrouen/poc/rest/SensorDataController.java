package com.univrouen.poc.rest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.univrouen.poc.model.LuminositeData;
import com.univrouen.poc.repositories.LuminositeDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.univrouen.poc.model.TemperatureData;
import com.univrouen.poc.repositories.TemperatureDataRepository;

@RestController
@RequestMapping("/sensorData")
public class SensorDataController {

	@Autowired
	private TemperatureDataRepository temperatureDataRepository;

	@Autowired
	private LuminositeDataRepository luminositeDataRepository;


	@GetMapping("/temperature")
	public List<TemperatureData> getTemperatureData() {
		List<TemperatureData> temperatureData = new ArrayList<>();
		temperatureDataRepository.findAll().forEach(temperatureData::add);
		return temperatureData;
	}
	
	@PostMapping("/temperature")
	public ResponseEntity<TemperatureData> addTemperatureData(@RequestBody TemperatureData temperatureData) {
		temperatureData.setDate(LocalDateTime.now());
		temperatureDataRepository.save(temperatureData);
		return ResponseEntity.ok(temperatureData);
	}


	@GetMapping("/luminosite")
	public List<LuminositeData> getUsers() {
		List<LuminositeData> luminositeDatas = new ArrayList<>();
		luminositeDataRepository.findAll().forEach(luminositeDatas::add);
		return luminositeDatas;
	}

	@PostMapping("/luminosite")
	public ResponseEntity<LuminositeData> addUser(@RequestBody LuminositeData luminositeData) {
		luminositeData.setDate(LocalDateTime.now());
		luminositeDataRepository.save(luminositeData);
		return ResponseEntity.ok(luminositeData);
	}
}
