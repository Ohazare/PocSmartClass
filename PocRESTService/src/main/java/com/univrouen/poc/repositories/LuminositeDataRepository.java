package com.univrouen.poc.repositories;

import com.univrouen.poc.model.LuminositeData;
import com.univrouen.poc.model.TemperatureData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LuminositeDataRepository extends CrudRepository<LuminositeData, Long>{

}
