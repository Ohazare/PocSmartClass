package com.univrouen.poc.repositories;
import org.springframework.data.repository.CrudRepository;

import com.univrouen.poc.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
