package com.univrouen.poc.repositories;

import org.springframework.stereotype.Repository;

import org.springframework.data.repository.CrudRepository;
import com.univrouen.poc.model.TemperatureData;


@Repository
public interface TemperatureDataRepository extends CrudRepository<TemperatureData, Long>{

}
