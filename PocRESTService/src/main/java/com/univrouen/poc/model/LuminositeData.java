package com.univrouen.poc.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class LuminositeData {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	@Column
	private LocalDateTime date;

	@Column
	private boolean luminosite;


	protected LuminositeData() {
		
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public LocalDateTime getDate() {
		return date;
	}


	public void setDate(LocalDateTime date) {
		this.date = date;
	}


	public boolean isLuminosite() {
		return luminosite;
	}

	public void setLuminosite(boolean luminosite) {
		this.luminosite = luminosite;
	}
}
