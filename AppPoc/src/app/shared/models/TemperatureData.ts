import { Date } from './date';

export class TemperatureData {
   constructor(public id: number = null,
     public temperature: number = 0,
     public date:Date = null) {}
}
