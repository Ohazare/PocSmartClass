import { Date } from './date';

export class LuminositeData {
   constructor(public id: number = null,
     public luminosite: boolean = false,
     public date:Date = null) {}
}
