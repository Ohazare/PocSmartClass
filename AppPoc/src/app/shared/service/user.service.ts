import { Injectable } from '@angular/core';
import { POC_SERVICE_URL } from './const';
import { Http, Headers, Response } from '@angular/http';
import { User } from '../models/user';


@Injectable()
export class UserService {

  private headers = new Headers({'Content-Type': 'application/json'});

  private userUrl = POC_SERVICE_URL + '/user';  // URL to web api

  constructor(private http: Http) { }

  getUsers(): Promise<User[]> {
    return this.http.get(this.userUrl)
             .toPromise()
             .then(response => response.json() as User[])
             .catch(this.handleError);
   }

  create(user: User): Promise<User> {
    return this.http
      .post(this.userUrl, JSON.stringify(user), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as User)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
