import { Injectable } from '@angular/core';
import { POC_SERVICE_URL } from './const';
import { Http, Headers, Response } from '@angular/http';
import { LuminositeData } from '../models/LuminositeData';

@Injectable()
export class LuminositeService {

    private headers = new Headers({'Content-Type': 'application/json'});

    private luminositeDataUrl = POC_SERVICE_URL + '/sensorData/luminosite';  // URL to web api

    constructor(private http: Http) { }

    getLuminositeDatas(): Promise<LuminositeData[]> {
      return this.http.get(this.luminositeDataUrl)
               .toPromise()
               .then(response => response.json() as LuminositeData[])
               .catch(this.handleError);
     }

    create(luminositeData: LuminositeData): Promise<LuminositeData> {
      return this.http
        .post(this.luminositeDataUrl, JSON.stringify(luminositeData), {headers: this.headers})
        .toPromise()
        .then(res => res.json() as LuminositeData)
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
    }
}
