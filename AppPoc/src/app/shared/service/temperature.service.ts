import { Injectable } from '@angular/core';
import { POC_SERVICE_URL } from './const';
import { Http, Headers, Response } from '@angular/http';
import { TemperatureData } from '../models/TemperatureData';

@Injectable()
export class TemperatureService {

    private headers = new Headers({'Content-Type': 'application/json'});

    private TemperatureDataUrl = POC_SERVICE_URL + '/sensorData/temperature';  // URL to web api

    constructor(private http: Http) { }

    getTemperatureDatas(): Promise<TemperatureData[]> {
      return this.http.get(this.TemperatureDataUrl)
               .toPromise()
               .then(response => response.json() as TemperatureData[])
               .catch(this.handleError);
     }

    create(TemperatureData: TemperatureData): Promise<TemperatureData> {
      return this.http
        .post(this.TemperatureDataUrl, JSON.stringify(TemperatureData), {headers: this.headers})
        .toPromise()
        .then(res => res.json() as TemperatureData)
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
    }
}
