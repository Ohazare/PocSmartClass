import { Component, OnInit } from '@angular/core';
import { UserService } from './shared/service/user.service'
import { User } from './shared/models/user'
import { TemperatureData } from './shared/models/TemperatureData'
import { TemperatureService } from './shared/service/temperature.service'
import { Date } from './shared/models/date'
import { LuminositeData } from './shared/models/LuminositeData'
import { LuminositeService } from './shared/service/luminosite.service'
import { Observable } from 'rxjs/Observable';

import Chart from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, TemperatureService, LuminositeService]
})
export class AppComponent implements OnInit {
  title = 'app';
  users: User[] = new Array<User>();
  userToAdd: User = new User();

  temperatureDatas: TemperatureData[] = new Array<TemperatureData>();
  graphicDataTemperatureDate: String[] = new Array<String>();
  graphicDataTemperature: number[] = new Array<number>();


  luminositeDatas: LuminositeData[] = new Array<LuminositeData>();
  graphicDataLuminositeDate: String[] = new Array<String>();
  graphicDataLuminosite: boolean[] = new Array<boolean>();

  constructor(private userService: UserService,
    private temperatureService: TemperatureService,
    private luminositeService: LuminositeService) {}

  ngOnInit(): void {

//    this.getUsers();
    this.refresh();
  }

  refresh(): void {
    this.getTemperatureDatas();
    this.getLuminositeDatas();
  }

  getUsers() : void {
    this.userService.getUsers().then(users => {
      this.users = users;
    });
  }
  addUser(user: User): void {
    this.userService.create(this.userToAdd).then(tmp => this.users.push(tmp));
  }

  getTemperatureDatas() : void {
    this.temperatureService.getTemperatureDatas().then(temperatureDatas => {
      this.temperatureDatas = temperatureDatas;
      this.refreshGraphicData();
    });

  }

  getLuminositeDatas() : void {
    this.luminositeService.getLuminositeDatas().then(luminositeDatas => {
      this.luminositeDatas = luminositeDatas;
      this.refreshGraphicData();
    });

  }


  refreshGraphicData(): void {
    this.graphicDataTemperatureDate = new Array<String>();
    this.graphicDataTemperature = new Array<number>();
    console.log(this.temperatureDatas);
    this.temperatureDatas.forEach((tmp) => {
        this.graphicDataTemperatureDate.push(this.formatDate(tmp.date));
        this.graphicDataTemperature.push(tmp.temperature);
    });

    this.graphicDataLuminositeDate = new Array<String>();
    this.graphicDataLuminosite = new Array<boolean>();
    console.log(this.luminositeDatas);
    this.luminositeDatas.forEach((tmp) => {
        this.graphicDataLuminositeDate.push(this.formatDate(tmp.date));
        this.graphicDataLuminosite.push(tmp.luminosite);
    });


    /*********************************************/
    /************CREATION DU GRAPHIQUE************/
    /*********************************************/
    // Utiliser graphicDataLuminosite: boolean[] et graphicDataLuminositeDate: Date[]
    // Pour mettre à jour les données du graphiques pour le capteur luminosite
    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: this.graphicDataTemperatureDate,
            datasets: [{
                data: this.graphicDataTemperature
            }]
        }
    });
  }






  formatDate(date: Date): String {
    return date.hour +"h:"+date.minute+"m:" +date.second+"s  , "+ date.dayOfMonth+"/" +date.monthValue +"/" + date.year;
  }
}
